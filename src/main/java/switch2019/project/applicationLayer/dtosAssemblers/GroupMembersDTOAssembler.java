package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.GroupMembersDTO;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;

import java.util.ArrayList;
import java.util.List;

public class GroupMembersDTOAssembler {

    public static GroupMembersDTO createDTOFromDomainObject(List<PersonID> members) {
        List<String> groupMembers = new ArrayList<>();

        for(PersonID personID : members){
            groupMembers.add(personID.getEmail().getEmail());
        }

        GroupMembersDTO groupMembersDTO = new GroupMembersDTO(groupMembers);
        return groupMembersDTO;
    }
}
