package switch2019.project.infrastructureLayer.repositoriesInMemory;

import org.springframework.stereotype.Repository;
import switch2019.project.domainLayer.domainEntities.aggregates.scheduling.Scheduling;
import switch2019.project.domainLayer.domainEntities.vosShared.ScheduleID;
import switch2019.project.domainLayer.repositoriesInterfaces.IScheduleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Ala Matos
 */
/*
 * Ala Matos created on 16/03/2020
 * inside the PACKAGE switch2019.project.repositories
 */

/**
 * Constructor of ScheduleRepository
 */
@Repository
public class ScheduleRepository implements IScheduleRepository {

    private List<Scheduling> schedulings;

    /**
     * Create schedule repository schedule repository.
     *
     * @return the schedule repository
     */
    public static ScheduleRepository createScheduleRepository() {
        return new ScheduleRepository();
    }

    public ScheduleRepository() {
        this.schedulings = new ArrayList<Scheduling>();
    }

    @Override
    public boolean saveScheduling(Scheduling scheduling) {
        if (!schedulings.contains(scheduling)) {
            return schedulings.add(scheduling);
        } else {
            return false;

        }
    }

    @Override
    public Scheduling findSchedulingByScheduleID(ScheduleID scheduleID) {
        for (Scheduling schedulingToAnalyze : schedulings) {
            if (schedulingToAnalyze.checkSchedulingID(scheduleID)) {
                return schedulingToAnalyze;
            }
        }
        return null;
    }

    @Override
    public int countSchedulings() {
        return schedulings.size();
    }

    @Override
    public boolean checkIfScheduleIDExists(ScheduleID scheduleID) {
        for (Scheduling schedulingToAnalyze : schedulings) {
            if (schedulingToAnalyze.checkSchedulingID(scheduleID)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Scheduling> getSchedulings() {
        return schedulings;
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScheduleRepository)) return false;

        ScheduleRepository that = (ScheduleRepository) o;

        List<Scheduling> schedulings = this.schedulings;
        List<Scheduling> otherScheduling = that.schedulings;

        if (schedulings.size() != otherScheduling.size()) {
            return false;
        }

        for(Scheduling schedulingToAnalyze  : schedulings) {
            if(!otherScheduling.contains(schedulingToAnalyze)){
                return false;
            }
        }
        return true;
    }


    /**
     * Hash code int.
     *
     * @return the int
     */
    @Override
    public int hashCode () {
        return Objects.hash(schedulings);
    }
}
