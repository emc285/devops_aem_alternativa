package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US002_1CreateGroupService;
import switch2019.project.applicationLayer.dtos.CreateGroupDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupDTOAssembler;

/**
 * The type Us 002 1 create group controller.
 *
 * @author Elisabete do Vale
 */

@Controller
public class US002_1CreateGroupController {

    @Autowired
    private final US002_1CreateGroupService us002_1CreateGroupService;

    //US002.1 Como utilizador, quero criar grupo, tornando-me administrador de grupo.

    /**
     * Instantiates a new Us 002 1 create group controller.
     *
     * @param us002_1CreateGroupService the us 002 1 create group service
     */
    public US002_1CreateGroupController(US002_1CreateGroupService us002_1CreateGroupService) {
        this.us002_1CreateGroupService = us002_1CreateGroupService;
    }

    /**
     * Create group as person in charge boolean dto.
     *
     * @param email        the email
     * @param denomination the denomination
     * @param description  the description
     * @return the boolean dto
     */
    public GroupDTO createGroupAsPersonInCharge(String email, String denomination, String description) {
        CreateGroupDTO createGroupDTO = CreateGroupDTOAssembler.createDTOFromPrimitiveTypes(email, denomination, description);
        return us002_1CreateGroupService.createGroupAsPersonInCharge(createGroupDTO);
    }
}
