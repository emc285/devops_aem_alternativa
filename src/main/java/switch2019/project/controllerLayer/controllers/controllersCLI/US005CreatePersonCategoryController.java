package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US005CreatePersonCategoryService;
import switch2019.project.applicationLayer.dtos.CreatePersonCategoryDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreatePersonCategoryDTOAssembler;

/**
 * The type Us 005 create person controller controller.
 */
@Controller
public class US005CreatePersonCategoryController {

    @Autowired
    private US005CreatePersonCategoryService us005CreatePersonCategoryService;

    /**
     * Instantiates a new Us 005 create person category controller.
     *
     * @param us005CreatePersonCategoryService the us 005 create person category service
     */
    public US005CreatePersonCategoryController(US005CreatePersonCategoryService us005CreatePersonCategoryService) {
        this.us005CreatePersonCategoryService = us005CreatePersonCategoryService;

    }

    /**
     * Create category boolean dto.
     *
     * @param email        the email
     * @param denomination the denomination
     * @return the boolean dto
     */
    public PersonDTO createCategory (String email, String denomination) {
        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(email, denomination);
        return us005CreatePersonCategoryService.createCategory(createPersonCategoryDTO);
    }

}
