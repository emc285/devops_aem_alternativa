package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US005_1CreateGroupCategoryService;
import switch2019.project.applicationLayer.dtos.CreateGroupCategoryDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupCategoryDTOAssembler;

/**
 * The type Us 005 1 create group category controller.
 */
@Controller
public class US005_1CreateGroupCategoryController {

    @Autowired
    private US005_1CreateGroupCategoryService us005_1CreateGroupCategoryService;

    //US005.1 Como responsável de grupo, quero criar categoria e associá-la ao grupo.

    /**
     * Instantiates a new Us 005 1 create group category controller.
     *
     * @param us005_1CreateGroupCategoryService the us 005 1 create group category service
     */
    public US005_1CreateGroupCategoryController(US005_1CreateGroupCategoryService us005_1CreateGroupCategoryService) {
        this.us005_1CreateGroupCategoryService = us005_1CreateGroupCategoryService;
    }

    /**
     * Create category as people in charge boolean dto.
     *
     * @param personEmail          the person email
     * @param groupDenomination    the group denomination
     * @param categoryDenomination the category denomination
     * @return the boolean dto
     */
    public GroupDTO createCategoryAsPeopleInCharge(String personEmail, String groupDenomination, String categoryDenomination) {
        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);
        return us005_1CreateGroupCategoryService.createCategoryAsPeopleInCharge(createGroupCategoryDTO);
    }


}
