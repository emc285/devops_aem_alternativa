package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US006CreatePersonAccountService;
import switch2019.project.applicationLayer.dtos.CreatePersonAccountDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreatePersonAccountDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US006CreatePersonAccountController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthdate;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthplace;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Name;
import switch2019.project.domainLayer.domainEntities.vosShared.Email;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

    /*
    US 06. Como utilizador, quero criar uma conta para mim, atribuindo-lhe uma
    denominação e uma descrição, para posteriormente poder ser usada nos meus movimentos.
     */

class US006CreatePersonAccountControllerTest extends AbstractTest {

    @Mock
    private US006CreatePersonAccountService service;

    // SUCCESS

    @Test
    @DisplayName("test for createAccount() | Success")
    public void whenPersonAccountIsCreated_thenRetrievedMsgIsSuccess() {

        // Arrange

        // Arrange Person
        final String personEmail = "manuel@gmail.com";
        final String personName = "Fontes";
        final LocalDate personBirthdate = LocalDate.of(1964, 02, 16);
        final String personBirthplace = "Vila Nova de Gaia";

        // Arrange Account
        final String accountDescription = "Tickets for Roland Garros";
        final String accountDenomination = "Tennis";

        // Expected result
        Email email = Email.createEmail(personEmail);
        Name name = Name.createName(personName);
        Birthdate birthdate = Birthdate.createBirthdate(personBirthdate);
        Birthplace birthplace = Birthplace.createBirthplace(personBirthplace);
        PersonID fatherID = null;
        PersonID motherID = null;

        PersonDTO isAccountCreatedExpected = PersonDTOAssembler.createDTOFromDomainObject(email, name, birthdate, birthplace, fatherID, motherID);

        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccount method, so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createAccount(createPersonAccountDTO)).thenReturn(isAccountCreatedExpected);

        // Controller
        US006CreatePersonAccountController controller = new US006CreatePersonAccountController(service);

        // Act
        PersonDTO result = controller.createAccount(personEmail, accountDescription, accountDenomination);

        // Assert
        assertEquals(isAccountCreatedExpected, result);
    }


    // ACCOUNT_ALREADY_EXIST - using account denomination & description from "Bootstrapping"

    @Test
    @DisplayName("test for createAccount() | Account Already Exists")
    public void whenPersonAccountIsCreated_thenRetrievedMsgIsAccountAlreadyExists() {

        //Arrange
        String personEmail = "manuel@gmail.com";
        String accountDescription = "Personal bank account";
        String accountDenomination = "Bank Account";

        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccount method, so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createAccount(createPersonAccountDTO)).thenThrow(new InvalidArgumentsBusinessException(US006CreatePersonAccountService.ACCOUNT_ALREADY_EXIST));

        // Controller
        US006CreatePersonAccountController controller = new US006CreatePersonAccountController(service);

        // Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createAccount(personEmail, accountDescription, accountDenomination));

        // Assert
        assertEquals(thrown.getMessage(), US006CreatePersonAccountService.ACCOUNT_ALREADY_EXIST);
    }


    // PERSON_DOES_NOT_EXIST

    @Test
    @DisplayName("test for createAccount() | Person Does Not Exist")
    public void whenPersonAccountIsCreated_thenRetrievedMsgIsPersonDoesNotExists() {

        // Arrange
        String personEmail = "manuel@gmail.com";
        String accountDescription = "Lemos Family sports expenses";
        String accountDenomination = "Tennis";

        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccount method, so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createAccount(createPersonAccountDTO)).thenThrow(new InvalidArgumentsBusinessException(US006CreatePersonAccountService.PERSON_DOES_NOT_EXIST));

        // Controller
        US006CreatePersonAccountController controller = new US006CreatePersonAccountController(service);

        // Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createAccount(personEmail, accountDescription, accountDenomination));

        // Assert
        assertEquals(thrown.getMessage(), US006CreatePersonAccountService.PERSON_DOES_NOT_EXIST);
    }

}
