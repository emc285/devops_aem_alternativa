package switch2019.project.controllerLayer.unitTests.controllersREST;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US001CheckIfSiblingsService;
import switch2019.project.applicationLayer.dtos.BooleanDTO;
import switch2019.project.applicationLayer.dtos.CheckIfSiblingsDTO;
import switch2019.project.applicationLayer.dtosAssemblers.BooleanDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.CheckIfSiblingsDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersREST.US001_CheckIfSiblingsControllerRest;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author Ala Matos
 */
public class US001_CheckIfSiblingsControllerRESTTest extends AbstractTest {
//
//    @Autowired
//    private US001_CheckIfSiblingsControllerRest us001_checkIfSiblingsControllerRest;
//    @Mock
//    private US001CheckIfSiblingsService us001CheckIfSiblingsService;
//
//    @Test
//    @DisplayName("Test Controller_US01 - hulk | wolverine | siblings")
//    void controller_US01() throws Exception {
//
////        Arrange people
//
//        String pauloEmail = "paulo@gmail.com";
//        String helderEmail = "helder@gmail.com";
//        String ruiEmail = "rui@gmail.com";
//
//
////        Arrange expected result
//
//        boolean result = true;
//        String expectedMsg = US001CheckIfSiblingsService.SUCCESS;
//        BooleanDTO expectedSiblingsAnalysis = BooleanDTOAssembler.createDTOFromPrimitiveTypes(result, expectedMsg);
//
//
////        Arrange checkIfSiblingsDTO
//
//        CheckIfSiblingsDTO checkIfSiblingsDTO = CheckIfSiblingsDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, helderEmail);
//
//        //Act
//        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
//        // so it does not depend on other parts (e.g. DB)
//        Mockito.when(us001CheckIfSiblingsService.checkIfSiblings(checkIfSiblingsDTO))
//                .thenReturn(expectedSiblingsAnalysis);
//
////      Act expected object
//        ResponseEntity<Object> expectedSiblingsResult = new ResponseEntity<>(expectedSiblingsAnalysis, HttpStatus.OK);
//
////      Act actual result
//        ResponseEntity<Object> actualResultSiblings = us001_checkIfSiblingsControllerRest.checkIfSiblings(pauloEmail, helderEmail);
//
////        Assert
//        assertEquals(expectedSiblingsResult, actualResultSiblings);
//    }

//    @Test
//    @DisplayName("Test Controller_US01 - hulk | ironMan | Not siblings")
//    void controller_US01NotSiblings() throws Exception {
//
////        Arrange people
//
//        String pauloEmail = "paulo@gmail.com";
//        String helderEmail = "helder@gmail.com";
//        String ruiEmail = "rui@gmail.com";
//
//
////        Arrange expected result
//
//        boolean resultNotSiblings = false;
//        String expectedMsgNotSiblings = US001CheckIfSiblingsService.FAIL;
//        BooleanDTO expectedNotSiblingsAnalysis = BooleanDTOAssembler.createDTOFromPrimitiveTypes(resultNotSiblings, expectedMsgNotSiblings);
//
////        Arrange checkIfSiblingsDTO
//        CheckIfSiblingsDTO checkINotSiblingsDTO = CheckIfSiblingsDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, ruiEmail);
//
////        Act
//        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
//        // so it does not depend on other parts (e.g. DB)
//        Mockito.when(us001CheckIfSiblingsService.checkIfSiblings(checkINotSiblingsDTO))
//                .thenThrow(new InvalidArgumentsBusinessException(US001CheckIfSiblingsService.FAIL));
//
////       Act expected object
////        ResponseEntity<Object> expectedNotSiblingsResult = new ResponseEntity<>(expectedNotSiblingsAnalysis, HttpStatus.OK);
//
////       Act actual result
//
//        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us001_checkIfSiblingsControllerRest.checkIfSiblings(pauloEmail, ruiEmail));
//
////        Assert
//
//        assertEquals(thrown.getMessage(), "Not Siblings");
//    }


}
